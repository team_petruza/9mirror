#! /usr/local/bin/python3
import os
import argparse
from PIL import Image

def main():
	ap = argparse.ArgumentParser()
	ap.add_argument("image", help="path to image")
	arg = vars(ap.parse_args())
	
	imagein = Image.open(arg["image"])
	
	imageout = Image.new( imagein.mode, size= ( imagein.width *3, imagein.height *3 ))
	imageout.paste( imagein, ( imagein.width, imagein.height ))
	
	flip_h = imagein.transpose( Image.FLIP_LEFT_RIGHT )
	imageout.paste( flip_h, ( 0, imagein.height ))
	imageout.paste( flip_h, ( imagein.width *2, imagein.height ))
	
	flip_v = imagein.transpose( Image.FLIP_TOP_BOTTOM )
	imageout.paste( flip_v, ( imagein.width, 0 ))
	imageout.paste( flip_v, ( imagein.width, imagein.height *2 ))
	
	flip_d = imagein.transpose( Image.ROTATE_180 )
	imageout.paste( flip_d, ( 0, 0 ))
	imageout.paste( flip_d, ( 0, imagein.height *2 ))
	imageout.paste( flip_d, ( imagein.width *2, 0 ))
	imageout.paste( flip_d, ( imagein.width *2, imagein.height *2 ))
	
	path, ext = os.path.splitext( os.path.normpath( arg["image"] ))
	imageout.save( path +".9mirror"+ ext )
	imageout.show()


if __name__ == '__main__':
	main()
